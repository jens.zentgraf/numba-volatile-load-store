import time
import numpy as np
import ctypes
import numba
from numba import njit
from concurrent.futures import ThreadPoolExecutor, as_completed

from volatile_load import generate_volatile_load, generate_volatile_store
volatile_load_i32 = generate_volatile_load("uint8")
volatile_store_i32 = generate_volatile_store("uint8")


test = np.zeros(1, dtype=np.uint32)

@njit(nogil=True)
def run(test):
    s = volatile_load_i32(test, 0)
    i = 0
    while not s:
        s = volatile_load_i32(test, 0)
        i+=1
    return i

@njit(nogil=True)
def stop(test):
    volatile_store_i32(test,0,1)
    return "stop"

with ThreadPoolExecutor(max_workers=2) as executor:
    futures = [
       executor.submit(run, test)]
    time.sleep(1)
    futures.append(executor.submit(stop, test))
    time.sleep(1)
    print(futures)

    for fut in as_completed(futures):
        print(fut.result())
print("ende")